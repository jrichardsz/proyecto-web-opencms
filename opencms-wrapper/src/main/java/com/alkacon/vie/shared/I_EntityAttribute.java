package com.alkacon.vie.shared;

import java.util.List;

public abstract interface I_EntityAttribute
{
  public abstract String getAttributeName();

  public abstract I_Entity getComplexValue();

  public abstract List<I_Entity> getComplexValues();

  public abstract String getSimpleValue();

  public abstract List<String> getSimpleValues();

  public abstract int getValueCount();

  public abstract boolean isComplexValue();

  public abstract boolean isSimpleValue();

  public abstract boolean isSingleValue();
}

/* Location:           X:\RLEON\Richard\CMS\Cms\OpenCms\git\001\lib\wrapper.jar
 * Qualified Name:     com.alkacon.vie.shared.I_EntityAttribute
 * JD-Core Version:    0.6.0
 */