package com.alkacon.vie.shared;

import java.util.List;

public abstract interface I_Entity
{
  public abstract void addAttributeValue(String paramString, I_Entity paramI_Entity);

  public abstract void addAttributeValue(String paramString1, String paramString2);

  public abstract I_Entity createDeepCopy(String paramString);

  public abstract I_EntityAttribute getAttribute(String paramString);

  public abstract List<I_EntityAttribute> getAttributes();

  public abstract String getId();

  public abstract String getTypeName();

  public abstract boolean hasAttribute(String paramString);

  public abstract void insertAttributeValue(String paramString, I_Entity paramI_Entity, int paramInt);

  public abstract void insertAttributeValue(String paramString1, String paramString2, int paramInt);

  public abstract void removeAttribute(String paramString);

  public abstract void removeAttributeSilent(String paramString);

  public abstract void removeAttributeValue(String paramString, int paramInt);

  public abstract void setAttributeValue(String paramString, I_Entity paramI_Entity);

  public abstract void setAttributeValue(String paramString, I_Entity paramI_Entity, int paramInt);

  public abstract void setAttributeValue(String paramString1, String paramString2);

  public abstract void setAttributeValue(String paramString1, String paramString2, int paramInt);

  public abstract String toJSON();
}

/* Location:           X:\RLEON\Richard\CMS\Cms\OpenCms\git\001\lib\wrapper.jar
 * Qualified Name:     com.alkacon.vie.shared.I_Entity
 * JD-Core Version:    0.6.0
 */