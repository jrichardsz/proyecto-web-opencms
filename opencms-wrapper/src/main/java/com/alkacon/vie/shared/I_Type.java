package com.alkacon.vie.shared;

import java.util.List;

public abstract interface I_Type
{
  public abstract void addAttribute(String paramString1, String paramString2, int paramInt1, int paramInt2);

  public abstract int getAttributeMaxOccurrence(String paramString);

  public abstract int getAttributeMinOccurrence(String paramString);

  public abstract List<String> getAttributeNames();

  public abstract I_Type getAttributeType(String paramString);

  public abstract String getAttributeTypeName(String paramString);

  public abstract int getChoiceMaxOccurrence();

  public abstract String getId();

  public abstract boolean isChoice();

  public abstract boolean isSimpleType();

  public abstract void setChoiceMaxOccurrence(int paramInt);
}

/* Location:           X:\RLEON\Richard\CMS\Cms\OpenCms\git\001\lib\wrapper.jar
 * Qualified Name:     com.alkacon.vie.shared.I_Type
 * JD-Core Version:    0.6.0
 */